<?php

namespace Drupal\orejime\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\orejime\Entity\Orejime;
use DrupalCodeGenerator\Exception\ExceptionInterface;
use DrupalCodeGenerator\Exception\SilentException;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

/**
 * Provide Drush commands for create Orejime entity.
 */
final class CreateCommands extends DrushCommands {

  use AutowireTrait;

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create Orejime entity.
   *
   * @throws \Exception
   *   Thrown when the options are not valid.
 */
  #[CLI\Command(name: 'orejime:create-entity', aliases: ['orejime:create'])]
  #[CLI\Argument(name: 'machine_name', description: 'A unique machine-readable name containing letters, numbers, and underscores.')]
  #[CLI\Argument(name: 'label', description: 'The label of the entity.')]
  #[CLI\Option(name: 'publish', description: 'Publish entity.')]
  #[CLI\Option(name: 'description', description: 'Describe the entity.')]
  #[CLI\Option(name: 'cookies', description: 'Comma-separated list of cookies')]
  #[CLI\Option(name: 'default', description: 'Enabled entity by default.')]
  #[CLI\Option(name: 'required', description: 'Required entity.')]
  #[CLI\Option(name: 'purposes', description: 'Entity\'s purposes')]
  #[CLI\Version(version: '12.5')]
  public function createEntity(string $machine_name, string $label, $options = [
    'publish' => FALSE,
    'description' => '',
    'cookies' => '',
    'default' => FALSE,
    'required' => FALSE,
    'purposes' => '',
    ]): void
  {

    $this->assertMachineName($machine_name);
    $this->assertTextlength($label, 'label');

    if (empty($options['description'])) {
      throw new \Exception('The "description" option cannot be empty.');
    }

    $this->assertTextlength($options['purposes'], 'purposes');

    try {
      $orejime = $this->entityTypeManager->getStorage('orejime_service')->create([
        'type' => 'orejime_system',
        'name' => $machine_name,
        'label' => $label,
        'status' => (bool) $options['publish'],
        'description' => $options['description'],
        'cookies' => str_replace(", ", "\n", $options['cookies']),
        'default' => (bool) $options['default'],
        'required' => (bool) $options['required'],
        'purposes' => $options['purposes'],
      ]);

      $orejime->save();
      $this->io()->success("'{$machine_name}' entity has been successfully created.");
    }
    catch (ExceptionInterface $exception) {
      if (!$exception instanceof SilentException) {
        $this->io()->getErrorStyle()->error($exception->getMessage());
      }
    }

  }

  /**
   * Validate that string doesn't exceed 255 characters.
   *
   * @param string $value
   *   The string.
   *
   * @param string $name
   *   The field name.
   * @throws \Exception
   *   Thrown when the string exceed 255 characters.
   */
  protected function assertTextlength(string $value, string $name)  {
    if (strlen($value) > 255) {
      throw new \Exception('The :param_name cannot exceed 255 characters.', [':param_name' => $name]);
    }
  }

  /**
   * Validate that machine name doesn't exist and is valid.
   *
   * @param string $machine_name
   *   The machine name.
   * @throws \Exception
   *   Thrown when the machine_name is invalid.
   */
  protected function assertMachineName(string $machine_name)  {
    $query = $this->entityTypeManager->getStorage('orejime_service')->getQuery();
    $query->accessCheck();
    $query->condition('name', $machine_name, '=');
    $result = $query->execute();

    if ($result) {
      throw new \Exception('System Name must be unique.');
    }

    if (!preg_match('/^[a-z0-9_]+$/', $machine_name)) {
      throw new \Exception('Invalid machine name. Use only lowercase letters, numbers, and underscores.');
    }

  }
}
