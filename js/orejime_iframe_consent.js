(function ($, Drupal, drupalSettings) {
  "use strict";
  Drupal.orejimeIframeConsent = Drupal.orejimeIframeConsent || {};

  class IframeConsent extends HTMLElement {
    #provider = undefined
    #cmp = ''
    #cmpCallbackSet = false

    constructor() {
      super()

      const poster = this.getAttribute('poster') || ''
      const noConsent = this.hasAttribute('no-consent')
      const title = this.getAttribute('title') || ''
      // suppression de l'attribut title, dont le contenu est désormais transféré dans le DOM
      this.removeAttribute('title')
      const alt = this.getAttribute('alt') || ''

      this.#cmp = this.getCmpName()
      this.#provider = this.getProvider()

      this.template = document.createElement('template')

      this.template.innerHTML = Drupal.orejimeIframeConsent.consentHtml({}, poster, alt, title)

      // On crée le shadow DOM
      this.attachShadow({ mode: 'open' })
      this.shadowRoot.appendChild(this.template.content.cloneNode(true))

      this.button = this.shadowRoot.querySelector('.iframe-poster')
      this.button.addEventListener('click', () => {
        if (!this.hasConsent(noConsent)) {
          return this.showBanner()
        }
        return this.setIframe()
      })
    }

    showBanner() {
      // On crée la bannière de consentement
      this.banner = document.createElement('div')
      this.banner.setAttribute('class', 'iframe-consent')

      if (this.#cmp) {
        this.banner.textContent = Drupal.t('Your consent preferences for %s do not allow you to access this content.').replace('%s', this.#provider)
      } else {
        this.banner.textContent = Drupal.t('By clicking on \"Continue\", you accept that the content provider (%s) may store cookies or trackers on your navigator.').replace('%s', this.#provider)
      }

      // On ajoute le bouton à la bannière
      const accept = document.createElement('button')
      if (this.#cmp) {
        accept.textContent = Drupal.t('Change your consent preferences')
        accept.addEventListener('click', () => this.openCmp().then(() => this.setIframe()))
      } else {
        accept.textContent = Drupal.t('Continue')
        accept.addEventListener('click', () => this.setIframe())
      }
      this.banner.appendChild(accept)

      this.shadowRoot.appendChild(this.banner)
    }

    setIframe() {
      if (this.banner) {
        this.banner.style.display = 'none'
      }
      this.button.style.display = 'none'

      const iframe = document.createElement('iframe')
      iframe.setAttribute('src', this.getAttribute('src'))
      iframe.setAttribute('frameborder', '0')
      iframe.setAttribute('allowfullscreen', '')
      iframe.setAttribute('allow', 'autoplay; encrypted-media; gyroscope')
      iframe.setAttribute('autoplay', '')

      this.shadowRoot.appendChild(iframe)
    }

    /**
     * @return {string}
     */
    getProvider() {
      let hostname = this.getAttribute('provider');
      if (this.#cmp === 'orejime') {
        const manager = window.orejime.internals.manager
        if (manager.getApp(hostname)) {
          return hostname
        } else {
          // la cmp ne gère pas ce provider, on repasse en mode standard
          this.#cmp = ''
          return Drupal.t('unknown')
        }
      }

      return hostname ||  Drupal.t('unknown')
    }

    hasConsent(noConsent) {
      if (noConsent) {
        return true
      }

      if (this.#cmp === 'orejime') {
        const manager = window.orejime.internals.manager
        return manager.getConsent(this.#provider)
      }

      return false
    }

    getCmpName() {
      if (window.orejime) {
        return 'orejime'
      }

      return ''
    }

    openCmp() {
      if (this.#cmp === 'orejime') {
        return new Promise((resolve) => {
          if (!this.#cmpCallbackSet) {
            document.addEventListener('orejimeAppCallback', (e) => {
              if (e.detail.app === this.#provider && e.detail.consent) {
                resolve()
              }
            })
            this.#cmpCallbackSet = true
          }
          window.orejime.show()
        })
      }

      return Promise.resolve(false)
    }
  }

  /**
   * @param {string} uri
   * @param {boolean} [keepExtension]
   * @return {string}
   */
  function canonicalHostname(uri, keepExtension = true) {
    uri = uri.trim().toLowerCase()
    if (uri === '' || !uri.includes('.')) {
      return ''
    }
    if (uri.startsWith('//')) {
      uri = 'https:' + uri
    } else if (!/^https?:\/\//.test(uri)) {
      uri = 'https://' + uri
    }

    const url = new URL(uri)
    let hostname = url.hostname.match(/([^.]+\.[^.]+)$/)[1]

    if (!keepExtension) {
      hostname = hostname.replace(/\.[^.]+$/, '')
    }

    return hostname
  }
  window.addEventListener("DOMContentLoaded",function() {
    customElements.define('iframe-consent', IframeConsent);
  });

  Drupal.orejimeIframeConsent.consentHtml = function (context, poster, alt, title) {
    return `
    <style>
    :host {
      display: block;
      position: relative;
      background-color: black;
      font-size: 1em;
      font-family: "Open Sans", sans-serif;
      line-height: 1.5;
      color: white;
    }
    a {
      color: white;
    }
    a:hover {
      text-decoration: none;
    }
    button {
      margin: 0;
      padding: 0;
      background: none;
      border: none;
      font-size: 1em;
      line-height: 1.5;
      color: inherit;
      display: inline-block;
      cursor: pointer;
    }
    img {
      max-width: 100%;
      vertical-align: middle;
    }
    iframe {
      position: absolute;
      inset: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
    }
  .iframe-poster {
      position: absolute;
      inset: 0;
      margin: 0;
      padding: 0;
      width: 100%;
      background: none;
      border: none;
      font-family: inherit;
      color: white;
      cursor: pointer;
      display: block;
    }
  .iframe-poster:has(.iframe-title)::before {
      position: absolute;
      inset: 0;
      z-index: 1;
      background: linear-gradient(to bottom, #0000 50%, #000);
      content: "";
      opacity: .6;
    }
  .iframe-img {
      position: absolute;
      inset: 0;
      width: 100%;
      height: 100%;
      object-fit: cover;
      opacity: .9;
    }
  .iframe-icon {
      margin-right: 1rem;
      opacity: .8;
      height: 1rem;
      vertical-align: middle;
      fill: #fff;
      transition: opacity .3s ease-in-out;
    }
  .iframe-title {
      position: absolute;
      inset: auto 0 0;
      z-index: 1;
      padding: 1rem;
      line-height: 1rem;
      font-weight: bold;
      text-align: left;
    }
  .iframe-poster:hover .iframe-icon {
      opacity: 1;
    }
  .iframe-consent {
      position: absolute;
      inset: 0;
      z-index: 3;
      display: grid;
      place-content: center;
      place-items: center;
      margin: 0;
      padding: 2rem;
      background: black;
      font-size: .875rem;
      color: white;
      text-align: center;
    }
  .iframe-consent button {
      padding: .5rem 1rem;
      margin-top: .875rem;
      background-color: white;
      border-radius: .25rem;
      font-weight: 700;
      color: black;
    }
  .iframe-consent button:hover {
      background-color: lightgrey;
    }
    </style>
    <button class="iframe-poster">
      <img src="${poster}" alt="${alt}" class="iframe-img">
      ${title ? `<span class="iframe-title"><svg class="iframe-icon" xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 384 512"><path d="M73 39c-14.8-9.1-33.4-9.4-48.5-.9S0 62.6 0 80V432c0 17.4 9.4 33.4 24.5 41.9s33.7 8.1 48.5-.9L361 297c14.3-8.7 23-24.2 23-41s-8.7-32.2-23-41L73 39z"/></svg>${title}</span>` : ''}
    </button>
    `
  }


  })(jQuery, Drupal, drupalSettings);
